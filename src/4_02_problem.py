from graph import Graph
import argparse

def loadProblem(file):
    with file as f:
        #Reading the first line with the number of vertices and edges
        first_line = f.readline()
        first_args = first_line.split(' ', maxsplit= 2)
        if len(first_args) != 2:
            raise Exception()
        n_vertices, n_edges = int(first_args[0]), int(first_args[1])
        graph = Graph(n_vertices, n_edges, undirected= True)

        #Reading the edges
        for _ in range(int(n_edges)):
            line = f.readline()
            args = line.split(' ', maxsplit= 3)
            if len(args) == 2:
                start, end = int(args[0]), int(args[1])
                graph.add_edge(start, end)
            else:
                start, end, weight = int(args[0]), int(args[1]), int(args[2])
                graph.add_edge(start, end, weight)


        #Reading the last line with the problem parameters
        #last_line = f.readline()
        #last_args = last_line.split(' ', maxsplit= 2)
        #start, end = int(last_args[0]), int(last_args[1])

    return graph#, start, end

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', type= argparse.FileType('r'))

    args = parser.parse_args()

    g= loadProblem(file= args.infile)
    print("Graph:")
    g.print()
    for v in g.vertices.keys():
        print(f"Graph has negative cycle from initial vertice {v}: {g.sortest_path_negative_cycle(v)}")





if __name__ == '__main__':
    main()
