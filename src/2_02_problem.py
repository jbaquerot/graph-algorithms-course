from graph import Graph
import argparse

def loadProblem(file):
    with file as f:
        #Reading the first line with the number of vertices and edges
        first_line = f.readline()
        first_args = first_line.split(' ', maxsplit= 2)
        if len(first_args) != 2:
            raise Exception()
        n_vertices, n_edges = int(first_args[0]), int(first_args[1])
        graph = Graph(n_vertices, n_edges, undirected= False)

        #Reading the edges
        for _ in range(int(n_edges)):
            line = f.readline()
            args = line.split(' ', maxsplit= 2)
            if len(args) == 2:
                start, end = int(args[0]), int(args[1])
            else:
                start, end, weight = int(args[0]), int(args[1]), int(args[2])
            graph.add_edge(start, end)

    return graph

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', type= argparse.FileType('r'))

    args = parser.parse_args()

    g = loadProblem(file= args.infile)

    print(f"topological ordering: {g.topological_sort()}")
    print("Graph:")
    g.print()



if __name__ == '__main__':
    main()
