from graph import Graph
import argparse
import math

def loadProblem(file):
    with file as f:
        #Reading the first line with the number of vertices and edges
        first_line = f.readline()
        first_args = first_line.split(' ', maxsplit= 1)
        if len(first_args) != 1:
            raise Exception()
        n_points = int(first_args[0])
        graph = Graph(n_points, n_edges= 0, undirected= True)

        #Reading the position of the points
        coords = []
        for v1_label in range(1, n_points+1):
            line = f.readline()
            args = line.split(' ', maxsplit= 3)
            if len(args) == 2:
                x1, y1 = int(args[0]), int(args[1])
                for v2_label, x2, y2 in coords:
                    d = math.sqrt((x1 - x2)**2 + (y1 - y2)**2)
                    graph.add_edge(v1_label, v2_label, d)
                coords.append((v1_label, x1, y1))
            else:
                raise Expection()


        #Reading the last line with the problem parameters
        #last_line = f.readline()
        #last_args = last_line.split(' ', maxsplit= 2)
        #start, end = int(last_args[0]), int(last_args[1])

    return graph#, start, end

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', type= argparse.FileType('r'))

    args = parser.parse_args()

    g= loadProblem(file= args.infile)
    print("Graph:")
    g.print()
    costs, parents = g.min_length_segment()
    print(f"Cost of minimum length segment: {sum(costs)}")
    print(f"Parents of minimum length segment: {parents}")


if __name__ == '__main__':
    main()
