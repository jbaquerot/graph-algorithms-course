from graph import Graph
import argparse

def loadProblem(file):
    with file as f:
        #Reading the first line with the number of vertices and edges
        first_line = f.readline()
        first_args = first_line.split(' ', maxsplit= 2)
        if len(first_args) != 2:
            raise Exception()
        n_vertices, n_edges = int(first_args[0]), int(first_args[1])
        graph = Graph(n_vertices, n_edges, undirected= True)

        #Reading the edges
        for _ in range(int(n_edges)):
            line = f.readline()
            args = line.split(' ', maxsplit= 2)
            if len(args) == 2:
                start, end = int(args[0]), int(args[1])
            else:
                start, end, weight = int(args[0]), int(args[1]), int(args[2])
            graph.add_edge(start, end)


        #Reading the last line with the problem parameters
        last_line = f.readline()
        last_args = last_line.split(' ', maxsplit= 2)
        start, end = int(last_args[0]), int(last_args[1])

    return graph, start, end

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', type= argparse.FileType('r'))

    args = parser.parse_args()

    g, start, end = loadProblem(file= args.infile)
    print("Graph:")
    g.print()

    v_start = g.find_vertices(start)[0]
    v_end = g.find_vertices(end)[0]
    dist, prev, _ = g.breadth_first_search(v_start)
    print(f"distances from {start}: {dist}")
    print(f"previous node for distances from {start} to {end}: {g.reconstruct_path(v_start, v_end, prev)}")
    print(f"minimum number of edges from {start} to {end}: {dist[v_end]}")




if __name__ == '__main__':
    main()
