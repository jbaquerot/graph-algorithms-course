from dataclasses import dataclass
import copy
from collections import deque, OrderedDict, Counter
from itertools import count
import heapq
import random

@dataclass
class Vertice:
    '''Vertice of a graph'''
    label: int
    cc: int = None #connected_component
    previsit: int = None
    postvisit: int = None

#@dataclass
#class Edge:
#    '''adjacent Edge between two nodes.'''
#    end: Vertice
#    weight: int = 0


class Graph(object):
    """Graph implementation with adjacent list"""

    def __init__(self, n_vertices, n_edges= 0, undirected= True):
        super(Graph, self).__init__()
        self.n_vertices = n_vertices
        self.n_edges = n_edges
        self.vertices = {i: Vertice(label= i) for i in range(1, self.n_vertices+1)}
        self.adj_list= {v: dict() for v in self.vertices.keys()}
        self.undirected = undirected
        self.clock = 1

    def add_edge(self, start, end, weight = 0):
        self.adj_list[start].update({end : weight})
        if self.undirected:
            self.adj_list[end].update({start : weight}) # The graph is undirected

    def get_list_of_edges(self):
        result = []
        for v, edges in self.adj_list.items():
            for u, w in edges.items():
                result.append((v, u, w))
        return result

    def explore(self, vertice, cc = 0):
        visited = set()
        def previsit(vertice):
            if self.vertices[vertice].previsit is None:
                self.vertices[vertice].previsit= self.clock
                self.clock += 1

        def postvisit(vertice):
            if self.vertices[vertice].postvisit is None:
                self.vertices[vertice].postvisit= self.clock
                self.clock += 1

        def explore_aux(vertice_aux, cc, visited_aux):
            #print(f"visited_aux: {visited_aux}")
            self.vertices[vertice_aux].cc = cc
            previsit(vertice_aux)
            visited_aux.add(vertice_aux)
            for v in self.adj_list[vertice_aux].keys():
                if v not in visited_aux:
                    #print(f"explore {v} not in {visited_aux}")
                    explore_aux(v, cc, visited_aux)
            postvisit(vertice_aux)
            return visited_aux

        return explore_aux(vertice, cc, visited)

    def depth_first_search(self):
        visited = []
        components = []
        connected_component = 0
        for idx, v in self.vertices.items():
            if idx not in visited:
                v_explored = self.explore(idx, cc= connected_component)
                components.append(v_explored)
                visited.extend(v_explored)
                connected_component += 1
        return components

    def is_cyclic(self):
        for v_start_label, adj_list in self.adj_list.items():
            for v_end_label in adj_list.keys():
                if v_start_label > v_end_label:
                    return True
        return False


    def topological_sort(self):
        self.depth_first_search()
        # sort vertices by reverse post-order
        v_post_ordered= [label for label, _ in sorted(self.vertices.items(), key= lambda item: item[1].postvisit, reverse= True)]
        return v_post_ordered


    def reverse(self):
        if self.undirected:
            return copy.deepcopy(self)
        g_reversed= Graph(self.n_vertices, self.n_edges, undirected= False)
        for v_start_label, adj_list in self.adj_list.items():
            for v_end_label in adj_list.keys():
                g_reversed.add_edge(v_end_label, v_start_label)
        return g_reversed

    def strong_connected_component(self):
        visited = set()
        components = []
        gr = self.reverse()
        #print(f"reverse graph:")
        #gr.print()
        v_post_ordered= self.topological_sort()
        #print(f"v_post_ordered: {v_post_ordered}")
        for label in v_post_ordered:
            if label not in visited:
                #print(f"label {label} not in visited: {visited}")
                v_explored= gr.explore(label)
                #print(f"v_explored: {v_explored}")
                components.append(set(v_explored) - visited)
                visited.update(v_explored)
                #print(f"visited: {visited}")
        return components

    def breadth_first_search(self, initial_node):
        dist = [float('inf')] * (self.n_vertices + 1) #ignore the 0 position
        sum_weights = 0
        prev = [None] * (self.n_vertices + 1) #ignore the 0 position
        dist[initial_node] = 0
        prev[initial_node] = 0
        queue = deque()
        queue.append(initial_node)
        while (len(queue) > 0):
            u = queue.pop()
            for v, weight in self.adj_list[u].items():
                if dist[v] == float('inf'):
                    queue.append(v)
                    dist[v] = dist[u] + 1
                    sum_weights = sum_weights + weight
                    prev[v] = u
            #print(f"prev: {prev[1:]}")
            #prev[initial_node]= u
        return dist[1:], prev[1:], sum_weights

    def reconstruct_path(self, initial_node, node, prev):
        result = []
        u = node
        while u is not None and u != initial_node:
            result.append(u)
            u = prev[u]

        return result[::-1] #reverse the list

    def sortest_path(self, initial_node): #Dijkstra
        '''
        Dijkstra Algorithm: Sortest path from initial_node to others nodes
        The graph has positive edges
        '''
        dist = [float('inf')] * (self.n_vertices)
        prev = [None] * (self.n_vertices)
        dist[initial_node - 1] = 0
        h = []
        for v, d in zip(self.vertices.keys(), dist):
            heapq.heappush(h, (d, v))
        while h:
            d_u, u = heapq.heappop(h)
            for v, weight in self.adj_list[u].items():
                if dist[v-1] > d_u + weight:
                    dist[v-1] = d_u + weight
                    prev[v-1] = u
                    heapq.heappush(h, (dist[v-1], v))
        return dist, prev

    def sortest_path_negative_cycle(self, initial_node):  #Algorithm BELLMAN-FORD
        '''
        Bellman-Ford Algorithm: Sortest path from initial_node to others nodes
        The graph has positive or negative edges. Also dectect if there is negative cycles
        '''
        dist = [float('inf')] * (self.n_vertices)
        next_v = [None] * (self.n_vertices)
        dist[initial_node - 1] = 0
        for i in range(self.n_vertices - 1):
            for u, edges in self.adj_list.items():
                for v, weight in edges.items():
                    if dist[v - 1] > dist[u - 1] + weight:
                        dist[v - 1] = dist[u - 1] + weight
                        #prev[v - 1] = u - 1
                        next_v[u - 1] = v - 1
        #detection fo negative_cycle
        negative_cycle= set()
        for u, edges in self.adj_list.items():
            for v, weight in edges.items():
                if (dist[v - 1] > dist[u - 1] + weight):
                    negative_cycle.add(v - 1)
                    #add the next vertices of the cicle
                    negative_cycle.update(self.reconstruct_path(v - 1 , next_v[v - 1], next_v))

        #update the weights to the negative cycle vertices
        for w in negative_cycle:
            dist[w] = float("-inf")

        return negative_cycle, dist

    def min_length_segment(self):
        '''
        Prim Algorithm to minimum spanning trees
        '''
        costs = [None] * (self.n_vertices)
        parents = [None] * (self.n_vertices)
        tiebreak = count().__next__ # Factory for tie-breaking values
        explored = set()
        start = random.choice(range(1, self.n_vertices))
        unexplored = [(0, tiebreak(), start)]
        costs[start - 1] = 0
        while unexplored:
            cost, _, winner = heapq.heappop(unexplored)
            if winner not in explored:
                explored.add(winner)
                costs[winner - 1] = cost
                for neighbour, cost in self.adj_list[winner].items():
                    if neighbour not in explored:
                        heapq.heappush(unexplored, (cost, tiebreak(), neighbour))
                        parents[neighbour - 1] = winner
        return costs, parents

    def set_of_adj_vertices(self, vertice):
        # return the set of vertices idx from tha adjacent list of vertice
        return {v_idx for v_idx in self.adj_list[vertice].keys()}

    def find_verts_contain_all(self, vertices):
        #Find al the set of vertices that contains all vertices
        #print(f"finding vertices that contain all vertices:{vertices}")
        result = set()
        for v_idx in self.vertices:
            if self.set_of_adj_vertices(v_idx).issuperset(vertices):
                result.add(v_idx)
        return result

    def find_cliques(self):

        cliques = Counter() #used to remove repeated cliques

        def find_cliques_aux(clique):
            # Find all cliques and are stored in cliques_aux
            h = []
            heapq.heappush(h, clique)
            #explored = set()
            new_cliques = []
            while h:
                cliq = heapq.heappop(h)
                adj_verts = self.find_verts_contain_all(cliq)
                if not adj_verts: # It cannot extend the clique
                    new_cliques.append(cliq)
                for adj_v in adj_verts:
                    #if (adj_v not in explored):
                        #explored.add(adj_v)
                    tmp_clique = cliq.copy()
                    tmp_clique.add(adj_v)
                    #new_cliques.append(tmp_clique)
                    heapq.heappush(h, tmp_clique)
                        #explored.remove(adj_v)


            return new_cliques

        v_post_ordered = self.topological_sort()
        for v in v_post_ordered:
            #explored.add(v)
            cliques.update([frozenset(clique) for clique in find_cliques_aux({v})]) # should be imputable (like frozenset)
        return [set(clique) for clique in cliques.keys()]




    def plot(self, fname= "../figures/tmp.png"):
        import networkx as nx
        import matplotlib.pyplot as plt
        G = nx.Graph()
        G.add_nodes_from([node.label for node in self.vertices.values()])
        G.add_weighted_edges_from(self.get_list_of_edges())

        pos = nx.spring_layout(G)
        nx.draw(G, pos)
        nx.draw_networkx_labels(G, pos, {node.label:node.label for node in self.vertices.values()})
        plt.savefig(fname= fname)
        plt.show()


    def print(self):
        for v, adjs in self.adj_list.items():
            print(self.vertices[v], ':', adjs)
