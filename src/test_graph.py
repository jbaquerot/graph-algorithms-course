import unittest
from graph import Graph
import math


def loadProblem(file_name, undirected= True, withArgs= True):
    with open(file_name, "r") as f:
        #Reading the first line with the number of vertices and edges
        first_line = f.readline()
        first_args = first_line.split(' ', maxsplit= 2)
        if len(first_args) != 2:
            raise Exception()
        n_vertices, n_edges = int(first_args[0]), int(first_args[1])
        graph = Graph(n_vertices, n_edges, undirected)

        #Reading the edges
        for _ in range(int(n_edges)):
            line = f.readline()
            args = line.split(' ', maxsplit= 2)
            if len(args) == 2:
                start, end = int(args[0]), int(args[1])
                graph.add_edge(start, end)
            else:
                start, end, weight = int(args[0]), int(args[1]), int(args[2])
                graph.add_edge(start, end, weight)

        if withArgs:
            #Reading the last line with the problem parameters
            last_line = f.readline()
            last_args = (int(arg) for arg in last_line.split(' ', maxsplit= 2))
            return graph, *last_args

        return graph

def loadTreeProblem(file_name, withArgs= False):
    with open(file_name, "r") as f:
        first_line = f.readline()
        first_args = first_line.split(' ', maxsplit= 1)
        if len(first_args) != 1:
            raise Exception()
        n_points = int(first_args[0])
        graph = Graph(n_points, n_edges= 0, undirected= True)

        #Reading the position of the points
        coords = []
        for v1_label in range(1, n_points+1):
            line = f.readline()
            args = line.split(' ', maxsplit= 3)
            if len(args) == 2:
                x1, y1 = int(args[0]), int(args[1])
                for v2_label, x2, y2 in coords:
                    d = math.sqrt((x1 - x2)**2 + (y1 - y2)**2)
                    graph.add_edge(v1_label, v2_label, d)
                coords.append((v1_label, x1, y1))
            else:
                raise Expection()

        if withArgs:
            #Reading the last line with the problem parameters
            last_line = f.readline()
            last_args = last_line.split(' ', maxsplit= 1)
            n_clusters = int(last_args[0])
            return graph, n_clusters

    return graph


class TestGraph(unittest.TestCase):
    """docstring for TestGraph."""

    def test_there_is_a_path(self):
        g, start, end = loadProblem("../data/1_01_sample1.txt")
        self.assertIn(end, g.explore(start))

        g, start, end = loadProblem("../data/1_01_sample2.txt")
        self.assertNotIn(end, g.explore(start))

    def test_connected_components(self):
        g = loadProblem("../data/1_02_sample1.txt", undirected= True, withArgs= False)
        self.assertEqual(len(g.depth_first_search()), 2)


    def test_has_cicle(self):
        g = loadProblem("../data/2_01_sample1.txt", undirected= False, withArgs= False)
        self.assertTrue(g.is_cyclic())

        g = loadProblem("../data/2_01_sample2.txt", undirected= False, withArgs= False)
        self.assertFalse(g.is_cyclic())

    def test_topological_sort(self):
        g = loadProblem("../data/2_02_sample1.txt", undirected= False, withArgs= False)
        self.assertEqual(g.topological_sort(), [4, 3, 1, 2])

        g = loadProblem("../data/2_02_sample2.txt", undirected= False, withArgs= False)
        self.assertEqual(g.topological_sort(), [4, 3, 2, 1])

        g = loadProblem("../data/2_02_sample3.txt", undirected= False, withArgs= False)
        self.assertEqual(g.topological_sort(), [5, 4, 3, 2, 1])

        g = loadProblem("../data/2_04_sample1.txt", undirected= False, withArgs= False)
        self.assertEqual(g.topological_sort(), [4, 3, 1, 2])

        g = loadProblem("../data/2_04_sample2.txt", undirected= False, withArgs= False)
        self.assertEqual(g.topological_sort(), [4, 3, 2, 1])

        g = loadProblem("../data/2_04_sample3.txt", undirected= False, withArgs= False)
        self.assertEqual(g.topological_sort(), [5, 4, 3, 2, 1])

    def test_strongly_connected_components(self):
        g = loadProblem("../data/2_03_sample1.txt", undirected= False, withArgs= False)
        self.assertEqual(len(g.strong_connected_component()), 2)

        g = loadProblem("../data/2_03_sample2.txt", undirected= False, withArgs= False)
        self.assertEqual(len(g.strong_connected_component()), 5)

    def test_minimum_number_of_edges(self):
        g, start, end = loadProblem("../data/3_01_sample1.txt", undirected= True, withArgs= True)
        dist, prev, _ = g.breadth_first_search(start)
        self.assertEqual(dist[end - 1], 2)

        g, start, end = loadProblem("../data/3_01_sample2.txt", undirected= True, withArgs= True)
        dist, prev, _ = g.breadth_first_search(start)
        self.assertEqual(dist[end - 1], float('inf'))

    def test_sortest_path(self):
        g, start, end = loadProblem("../data/4_01_sample1.txt", undirected= False, withArgs= True)
        dist, prev = g.sortest_path(start)
        self.assertEqual(dist[end - 1], 3)

        g, start, end = loadProblem("../data/4_01_sample2.txt", undirected= False, withArgs= True)
        dist, prev = g.sortest_path(start)
        self.assertEqual(dist[end - 1], 6)

        g, start, end = loadProblem("../data/4_01_sample3.txt", undirected= False, withArgs= True)
        dist, prev = g.sortest_path(start)
        self.assertEqual(dist[end - 1], float("inf"))

    def test_sortest_path_negative_cycle(self):
        g = loadProblem("../data/4_02_sample1.txt", undirected= False, withArgs= False)
        result = any([g.sortest_path_negative_cycle(v) for v in g.vertices.keys()])
        self.assertTrue(result)

    def test_sortest_path_negative_cycle2(self):
        g, start = loadProblem("../data/4_03_sample1.txt", undirected= False, withArgs= True)
        negative_cycle, dist = g.sortest_path_negative_cycle(start)
        self.assertEqual(dist, [0, 10, float("-inf"), float("-inf"), float("-inf"), float("inf")])

        g, start = loadProblem("../data/4_03_sample2.txt", undirected= False, withArgs= True)
        negative_cycle, dist = g.sortest_path_negative_cycle(start)
        self.assertEqual(dist, [float("-inf"), float("-inf"), float("-inf"), 0, float("inf")])

    def test_min_length_segment(self):
        g = loadTreeProblem("../data/5_01_sample1.txt")
        costs, parents = g.min_length_segment()
        self.assertAlmostEqual(sum(costs), 3.0)

        g = loadTreeProblem("../data/5_01_sample2.txt")
        costs, parents = g.min_length_segment()
        self.assertAlmostEqual(sum(costs), 7.064495102)

    def test_clustering(self):
        g, n_clusters = loadTreeProblem("../data/5_02_sample1.txt", withArgs= True)
        costs, parents = g.min_length_segment()
        largest_cost = sorted(costs, reverse= True)[n_clusters - 2]
        self.assertAlmostEqual(largest_cost, 2.828427124746)

        g, n_clusters = loadTreeProblem("../data/5_02_sample2.txt", withArgs= True)
        costs, parents = g.min_length_segment()
        largest_cost = sorted(costs, reverse= True)[n_clusters - 2]
        self.assertAlmostEqual(largest_cost, 5)

    def test_get_list_of_edges(self):
        g = loadProblem("../data/2_01_sample1.txt", undirected= False, withArgs= False)
        list_edges = g.get_list_of_edges()
        self.assertEqual(list_edges, [(1, 2, 0), (2, 3, 0), (3, 1, 0), (4, 1, 0)])

        g = loadProblem("../data/1_01_sample1.txt", undirected= False, withArgs= False)
        list_edges = g.get_list_of_edges()
        self.assertEqual(list_edges, [(1, 2, 0), (1, 4, 0), (3, 2, 0), (4, 3, 0)])

    def test_plot(self):
        #g = loadProblem("../data/2_01_sample1.txt", undirected= False, withArgs= False)
        #g.plot("../figures/2_01_sample1.png")

        g = loadProblem("../data/2_01_sample1.txt", undirected= True, withArgs= False)
        g.plot("../figures/2_01_sample1.png")

    def test_set_of_adj_vertices(self):
        g = loadProblem("../data/2_01_sample2.txt", undirected= True, withArgs= False)
        self.assertEqual(g.set_of_adj_vertices(1), {2, 3, 4})
        self.assertEqual(g.set_of_adj_vertices(5), {2, 3})

        g = loadProblem("../data/2_02_sample3.txt", undirected= True, withArgs= False)
        self.assertEqual(g.set_of_adj_vertices(1), {2, 3, 4})
        self.assertEqual(g.set_of_adj_vertices(4), {1, 3})

        g = loadProblem("../data/1_01_sample1.txt", undirected= True, withArgs= False)
        self.assertEqual(g.set_of_adj_vertices(1), {2, 4})
        self.assertEqual(g.set_of_adj_vertices(2), {1, 3})
        self.assertEqual(g.set_of_adj_vertices(4), {1, 3})

    def test_find_verts_contain_all(self):
        g = loadProblem("../data/2_01_sample1.txt", undirected= True, withArgs= False)
        self.assertEqual(g.find_verts_contain_all({1, 2}), {3})
        self.assertEqual(g.find_verts_contain_all({1, 2, 3}), set())
        self.assertEqual(g.find_verts_contain_all({4}), {1})
        self.assertEqual(g.find_verts_contain_all({2, 4}), {1})
        self.assertEqual(g.find_verts_contain_all({1, 2, 4}), set())

        g = loadProblem("../data/2_02_sample3.txt", undirected= True, withArgs= False)
        self.assertEqual(g.find_verts_contain_all({1, 2}), {3})

    def test_find_cliques(self):
        g = loadProblem("../data/2_01_sample1.txt", undirected= True, withArgs= False)
        self.assertEqual(g.find_cliques(), [{1, 4}, {1, 2, 3}])
        #print(g.find_cliques())
        g = loadProblem("../data/2_01_sample2.txt", undirected= True, withArgs= False)
        self.assertEqual(g.find_cliques(), [{1, 2, 3}, {1, 3, 4}, {2, 3, 5}])

        g = loadProblem("../data/1_01_sample1.txt", undirected= True, withArgs= False)
        self.assertEqual(g.find_cliques(), [{1, 2}, {1, 4}, {2, 3}, {3, 4}])


if __name__ == '__main__':
    unittest.main()
